/*
This class will parse a file and provide 
a few more methods that are useful for the 
file checking
*/

import java.util.*;
import java.io.*;

class FileParse{

	private File file;
	private Scanner input;
	
	public FileParse(File file){
		this.file = file;
	}
	
	public FileParse(){
	}
	
	public void setFile(File file){
		this.file = file;
	}

	/*******************************************
	   puts each word in a file in output<i>
	   where i is the i(th) line of the file
	*******************************************/
	public ArrayList<String> parseToString() throws FileNotFoundException{
		input = new Scanner(file);
		
		String hold = null;
		String buffer = "";
		ArrayList<String> output = new ArrayList<String>();
		int c = 0;
		boolean flag = false;
		while(input.hasNextLine()){
			hold = input.nextLine();
			buffer = "";
			if(hold.length() == 1 && !flag){
				buffer = hold;
			}
				
			for(int i = 0; i < hold.length()-1; i++){
				if(hold.charAt(i) == '/'){
					if(hold.charAt(i+1) == '/' && !flag)
						break;
						
					else if(hold.charAt(i+1) == '*'){
						flag = true; //in a comment block
						continue;
					}
						
				}
				if(hold.charAt(i) == '*' && flag){
					if(hold.charAt(i+1) == '/'){
						flag = false; //leaving comment block
						++i; //ignore the straggling '/'
						continue;
					}
					
				}
				
				if(!flag)
					buffer += hold.charAt(i);
				if(i == hold.length()-2 && !flag)
					buffer+= hold.charAt(i+1);//make sure we're getting the last character in a line
			}
			
			if(!buffer.equals("")){
				output.add(buffer);
				c++; //what this should be written in
			}
		
		}
		
		return output;
		
			
		
	}
}
	
	
