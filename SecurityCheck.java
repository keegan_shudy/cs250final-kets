/*
This will take the arraylist from 
file parse and check it for security
flaws; including: unclosed input streams, incorrectly scoped variables
*/
import java.util.*;
class SecurityCheck{


	public static final String [] STREAM = {"AbstractInterruptibleChannel", 
		                        "AbstractSelectableChannel", "AbstractSelector", "AsynchronousFileChannel",
		                        "AsynchronousServerSocketChannel", "AsynchronousSocketChannel", 
		                        "AudioInputStream", "BufferedInputStream", "BufferedOutputStream", "BufferedReader",
		                        "BufferedWriter", "ByteArrayInputStream", "ByteArrayOutputStream", "CharArrayReader",
		                        "CharArrayWriter", "CheckedInputStream", "CheckedOutputStream", "CipherInputStream",
		                        "CipherOutputStream", "DatagramChannel", "DatagramSocket", "DataInputStream",
		                        "DataOutputStream", "DeflaterInputStream", "DeflaterOutputStream", "DigestInputStream",
		                        "DigestOutputStream", "FileCacheImageInputStream", "FileCacheImageOutputStream", 
		                        "FileChannel", "FileImageInputStream", "FileImageOutputStream", "FileInputStream",
		                        "FileLock", "FileOutputStream", "FileReader", "FileSystem", "FileWriter", "FilterInputStream",
		                        "FilterOutputStream", "FilterReader", "FilterWriter", "Formatter", "ForwardingJavaFileManager",
		                        "GZIPInputStream", "GZIPOutputStream", "ImageInputStreamImpl", "ImageOutputStreamImpl",
		                        "InflaterInputStream", "InflaterOutputStream", "InputStream", "InputStream", "InputStream",
		                        "InputStreamReader", "JarFile", "JarInputStream", "JarOutputStream", "LineNumberInputStream",
		                        "LineNumberReader", "LogStream", "MemoryCacheImageInputStream", "MemoryCacheImageOutputStream",
		                        "MLet", "MulticastSocket", "ObjectInputStream", "ObjectOutputStream", "OutputStream", 
		                        "OutputStream", "OutputStream", "OutputStreamWriter", "Pipe.SinkChannel", "Pipe.SourceChannel",
		                        "PipedInputStream", "PipedOutputStream", "PipedReader", "PipedWriter", "PrintStream", 
		                        "PrintWriter", "PrivateMLet", "ProgressMonitorInputStream", "PushbackInputStream",
		                        "PushbackReader", "RandomAccessFile", "Reader",
		                        "RMIConnectionImpl", "RMIConnectionImpl_Stub", "RMIConnector", "RMIIIOPServerImpl", 
		                        "RMIJRMPServerImpl", "RMIServerImpl", "Scanner", "SelectableChannel", "Selector",
		                        "SequenceInputStream", "ServerSocket", "ServerSocketChannel", "Socket",
		                        "SocketChannel", "SSLServerSocket", "SSLSocket", "StringBufferInputStream", "StringReader", "StringWriter",
		                        "URLClassLoader", "Writer", "XMLDecoder", "XMLEncoder", "ZipFile", "ZipInputStream", "ZipOutputStream"
					};

	private ArrayList<String> file = new ArrayList<String>();
	
	public SecurityCheck(){
	}
	
	public SecurityCheck(ArrayList<String> file){
		this.file = file;
	}
	
	public void setFile(ArrayList<String> file){
		this.file = file;
	}
	
	
	
	
	public ArrayList<String> checkStream(String Stream){
		ArrayList<String> scanners = populateStream(Stream);
		if(scanners.size() < 1)
			return null;
			
		Scanner line = null;
		
		
		for(String scannerName : scanners){
			
			boolean closed = false;
			//String scannerName = scanners.get(j);
			for(int i = 0; i < file.size(); i++){
				line = new Scanner(file.get(i));
				if(line.findWithinHorizon(scannerName + ".close()",0) != null && !closed){
					scanners.set(scanners.indexOf(scannerName), "");//yes, i know arraylist has remove(obj elem)
					
					closed = true;
					break;
				}
			}
		}
		line.close();
		scanners = trim(scanners);
		return scanners;
	}
	
	//returns a list of all var names that are incorrectly scoped
	public ArrayList<String> checkScope(){
		ArrayList<String> scoped = new ArrayList<String>();
		ArrayList<String> output = new ArrayList<String>();
		Scanner line = null;
		String hold = "";
		//boolean methodForVarl = false;
		
		//used for populating the arraylist with all method and var names with public scope
		for(int i = 0; i < file.size(); i++){
			hold = file.get(i).toLowerCase();
			line = new Scanner(hold);
			if(line.findWithinHorizon("public",0) != null /*&& hold.charAt(hold.length()-1) == ';'*/){
				scoped.add(getName2(hold,"public"));
			}
		}
		line.close();
		Scanner input = null;
		//check if a var in scoped has a matching setVar() method
		for(String method : file){
			input = new Scanner(method.toLowerCase());
			for(String var : scoped){
				String hold2 = "return " + (var.replaceAll("[^a-zA-Z ]", "")) + ";";
				if(input.findWithinHorizon(hold2,0) != null)
				   	output.add(var.substring(0,var.length()-1)); //remove the ; at the end
			}
		}
		input.close();
		return output;
	}
	
	
	
	
	/**************************************
	  used to get rid of the "" elements
	  left over from checkStream
	**************************************/
	private ArrayList<String> trim(ArrayList<String> streams){
		ArrayList<String> output = new ArrayList<String>();
		for(int i = 0; i < streams.size(); i++){
			if(streams.get(i).equalsIgnoreCase("") || streams.get(i).equals(" "))
				streams.remove(i);
		}
		for(int i = 0; i < streams.size(); i++)
			output.add(streams.get(i));
		return output;
	}
	
	
	
	
	
	
	/*********************************
	   potentially unnecessary, but
	   is used to populate an arraylist
	   with the names of all streamed
	   vars
	*********************************/
	
	private ArrayList<String> populateStream(String stream){
		String hold = "";
		Scanner line = null;
		String name = "";
		ArrayList<String> scanners = new ArrayList<String>();
		for(int i = 0; i < file.size(); i++){
			hold = file.get(i);
			hold = hold.replaceAll("[^a-zA-Z ]", ""); //remove any {, (), and ; for evaluation purposes
			line = new Scanner(hold);
			if(line.findWithinHorizon(stream,0) != null){
				name = getName(hold, stream);
				
				if(name == null) continue;
				
				scanners.add(name);
				
				//System.out.println("Found a scanner in line: " + (i+1) + " with name: " + name); //debug output
				
			}
		}
		line.close();
		return scanners;
	}
	
	/********************************
	   this will be used to obtain
	   the name of the streamed var
	********************************/
	private String getName(String line, String inputStream){
		line = line.trim(); //important for removing leading spaces
		String[] hold = line.split(" "); //turn the line into individual words
		for(int i = 0; i < hold.length-1; i++){
			if(hold[i].equals(inputStream)){
				return hold[i+1];
			}
		}
		return null; //this case shouldn't really happen since this should only be called when there is a know stream in the line
	}
	
	private String getName2(String line, String inputStream){
		line = line.trim(); //important for removing leading spaces
		String[] hold = line.split(" "); //turn the line into individual words
		for(int i = 0; i < hold.length-2; i++){
			if(hold[i].equals(inputStream)){
				return hold[i+2]; //+2 because a var in a class is [scope][type][var name]
			}
		}
		return null; //this case shouldn't really happen since this should only be called when there is a know stream in the line
	}
	
}
